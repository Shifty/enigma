
## Enigma Machine Configuration

Navigate to `internals/config.yml` and open it for editing.

```yml
alphabet: ABCDEFGHIJKLMNOPQRSTUVWXYZ
reflector: YRUHQSLDPXNGOKMIEBFZCWVJAT

rotor_a:
  cont: EKMFLGDQVZNTOWYHXUSPAIBRCJ
  turn: Q
  pos: 0

rotor_b:
  cont: AJDKSIRUXBLHWTMCQGZNPYFVOE
  turn: E
  pos: 0

rotor_c:
  cont: BDFHJLCPRTXVZNYEIWGAKMUSQO
  turn: V
  pos: 0

plugboard:
 - AB
```

* `alphabet`: Sets the base alphabet from which all indexes will be based on.
* `reflector`: Sets the letter mapping for the reflector plate.

* `rotor_a`: The left-most rotor.
    * `cont`: Sets the letter mapping.
    * `turn`: Sets the turning letter.
    * `pos`: Sets the starting position.

* `rotor_b`: The middle rotor.
    * `cont`: Sets the letter mapping.
    * `turn`: Sets the turning letter.
    * `pos`: Sets the starting position.

* `rotor_c`: The right-most rotor.
    * `cont`: Sets the letter mapping.
    * `turn`: Sets the turning letter.
    * `pos`: Sets the starting position.

* `plugboard`: Swaps letters, both when entering the rotors and when leaving them.
    * `- AB` Swaps the letters A and B.


> **Notes:**
> 1. Indices start at 0, rather than 1. So `pos: 0` is the beginning and `pos: 25` is the end.  
> 2.  Turning letters are letters that, when turned past, actuate the turning of the next-left rotor.
