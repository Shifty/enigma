import yaml

with open('internals/config.yml', encoding='utf-8') as rotor_file:
    data = yaml.safe_load(rotor_file)

rotor_a = data.get('rotor_a')
rotor_b = data.get('rotor_b')
rotor_c = data.get('rotor_c')
plugboard = data.get("plugboard") or []


def config_text():
    text = [
        'ALPHABET:\n',
        f'Contents: {data.get("alphabet")}\n\n',
        'ROTOR A:\n',
        f'Contents: {rotor_a.get("cont")}\n',
        f'Turnover: {rotor_a.get("turn")}\n',
        f'Position: {rotor_a.get("pos")}\n',
        'ROTOR B:\n',
        f'Contents: {rotor_b.get("cont")}\n',
        f'Turnover: {rotor_b.get("turn")}\n',
        f'Position: {rotor_b.get("pos")}\n\n',
        'ROTOR C:\n',
        f'Contents: {rotor_c.get("cont")}\n',
        f'Turnover: {rotor_c.get("turn")}\n',
        f'Position: {rotor_c.get("pos")}\n\n',
        'REFLECTOR:\n',
        f'Contents: {data.get("reflector")}\n\n',
        'PLUGBOARD:\n',
        f'Plugs: {", ".join(plugboard)}'
    ]
    return ''.join(text)


def help_text():
    text = [
        'To Encipher: Enter a message and press enter.\n',
        'To Decipher: Enter an encrypted message with the same settings that were used to encrypt it.\n',
        'Only characters in the ISO basic Latin alphabet will be processed.\n',
        'For configuration please see the config.md file.'
    ]
    return ''.join(text)
