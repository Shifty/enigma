import yaml

with open('internals/config.yml', encoding='utf-8') as rotor_file:
    rotor_data = yaml.safe_load(rotor_file)


class Rotor(object):
    def __init__(self, data):
        self.alpha = rotor_data.get('alphabet')
        self.cont = data.get('cont')
        self.pos = data.get('pos')
        self.turn = list(data.get('turn'))
        self.turn_index = [self.alpha.index(t) for t in self.turn]

    def at_turn(self):
        for t in self.turn_index:
            if self.pos == t + 1:
                return True
            elif t == 25 and self.pos == 0:
                return True

    def advance(self):
        self.pos += 1
        if self.pos > 25:
            self.pos = 0


class Rotors(object):
    def __init__(self):
        self.alpha = rotor_data.get('alphabet')

        self.a = Rotor(rotor_data.get('rotor_a'))
        self.b = Rotor(rotor_data.get('rotor_b'))
        self.c = Rotor(rotor_data.get('rotor_c'))

        self.reflector = rotor_data.get('reflector')
        self.plugboard = rotor_data.get('plugboard') or []

    def get_alpha_cont(self, rotor):
        return self.alpha[rotor.pos:] + self.alpha[:rotor.pos]

    @staticmethod
    def get_rotor_cont(rotor):
        return rotor.cont[rotor.pos:] + rotor.cont[:rotor.pos]

    def cycle(self):
        self.c.advance()

        b_before = self.b.pos
        if self.c.at_turn():
            self.b.advance()

        elif self.b.pos in self.b.turn_index:
            self.b.advance()

        if self.b.at_turn():
            if b_before != self.b.pos:
                self.a.advance()

    def get_plugboard(self, char):
        for plug in self.plugboard:
            if len(plug) == 2:
                if char.upper() in plug:
                    char = plug.replace(char.upper(), '')
                    break
        return char

    def encipher(self, deciphered):
        enciphered = ''
        for char in deciphered:
            if char.isalpha():
                self.cycle()
                ch_in = self.get_plugboard(char).upper()
                ch_in = ch_in.upper()
                for rotor in [self.c, self.b, self.a]:
                    alpha_cont = self.get_alpha_cont(rotor)
                    ch_in = alpha_cont[self.alpha.index(ch_in)]
                    ch_in = rotor.cont[self.alpha.index(ch_in)]
                    ch_in = self.alpha[alpha_cont.index(ch_in)]
                ch_in = self.reflector[self.alpha.index(ch_in)]
                for rotor in [self.a, self.b, self.c]:
                    alpha_cont = self.get_alpha_cont(rotor)
                    ch_in = alpha_cont[self.alpha.index(ch_in)]
                    ch_in = self.alpha[rotor.cont.index(ch_in)]
                    ch_in = self.alpha[alpha_cont.index(ch_in)]

                char_out = self.get_plugboard(ch_in)
                enciphered += char_out
        return enciphered
